#Calculothon#

Le tutorat à distance en mathématiques peut être une tâche laborieuse à accomplir. Le projet Calculothon a pour objectif d'offrir une application web permettant de partager une session de calcul privé entre deux utilisateurs où ils pourront utiliser une calculatrice commune et des messages vocaux.

###Getting started###

1. Pull project
2. Open terminal
3. Navigate to the project folder
4. Run "npm install"
5. Run "npm run serve"
6. ??
7. Profit

###Technologies utilisées###

* [Nodes.js](https://nodejs.org/en/)
* [AngularJS](https://angular.io/)
* [Bootstrap](http://getbootstrap.com/)
* [Html5](http://www.w3schools.com/html/html5_intro.asp)
* [Css3](http://www.w3schools.com/css/css3_intro.asp)

###Contributeurs###

* [Gabriel Beaulieu](https://bitbucket.org/gabriel_beaulieu/)
* [Philippe Richard](https://bitbucket.org/pric/)
* [Charles-Antoine Chartrand](https://bitbucket.org/Gladhus/)
* [Daniel Enachescu](https://bitbucket.org/Denachescu/)
* [Marc Olivier Bergeron](https://bitbucket.org/MOBergeron/)
* [Marc Grenier](https://bitbucket.org/MarcGrenier/)
