
angular.module('calculothon')
.controller('roomController', function ($scope, $http, $location, $timeout, $routeParams, roomService) {

	$scope.operations = '';
	$scope.calculatorHistory = [];
	$scope.inRoom = true;
	var mediaRecorder;
	var audioFiles = [];
	var audioFilesPlaying = {};
	var audioFilesPlayed = {};

	function digestDecorator(func) {
		return function() {
			func.apply(this, arguments);
			$timeout($scope.$digest());
		}
	}

	function audioOperation(arrayBuffer) {
		var blob = new Blob([arrayBuffer], { 'type' : 'audio/ogg; codecs=opus' });
		var audio = document.createElement('audio');
		audio.src = window.URL.createObjectURL(blob);
		audio.controls = true;

		document.getElementById('audiotest').appendChild(audio);
		audioFiles.unshift(audio);
		audioFilesPlaying[audio]=false;
		audioFilesPlayed[audio]=false;
		
		audio.onplaying = function(){
			audioFilesPlaying[audio]=true;
		}

		audio.onpause = function(){
			console.log("Paused");
			audioFilesPlayed[audio]=true;
			playAudio();
		}

		playAudio();
	};

	function playAudio(){
		for (var i = audioFiles.length - 1; i >= 0; i--) {
			var audio = audioFiles[i];
			
			if(audioFilesPlayed[audio]){
				audioFiles.pop();
				delete audioFilesPlayed[audio];
				delete audioFilesPlaying[audio];
			}
			else if(audioFilesPlaying[audio]){
				break;
			}
			else{
				audio.play();
				break;
			}
		};
	}

	function addOperation(operation) {
		$scope.operations += operation.key;
	}

	function ovewriteOperations(operation) {
		$scope.operations = operation.operations;
	}

	function eraseOperation() {
		$scope.operations = $scope.operations.substring(0, $scope.operations.length - 1);
	}

	function clearOperations() {
		$scope.operations = '';
	}

	function submitOperations(result) {
		var operationHistory = document.getElementById('operationHistory');
		
		var li = document.createElement('li');
		li.innerHTML = result.operations + ' = ' + result.result;
		operationHistory.appendChild(li);
		operationHistory.scrollTop = operationHistory.scrollHeight;

		$scope.operations = '';
	}

	$scope.addOperation = function(operation) {
		roomService.sendKeyPressed(operation.key);
	};

	$scope.eraseOperation = function() {
		roomService.sendErase();
		eraseOperation();
	};

	$scope.clearOperations = function() {
		roomService.sendClear();
	};

	$scope.submitOperations = function(broadcast) {
		roomService.sendSubmit();
	};

	$scope.audioOperation = function(){
		navigator.mediaDevices
			.getUserMedia({audio:true})
			.then(function(mediaStream){
				mediaRecorder = new MediaRecorder(mediaStream);

				this.start=0; 

				mediaRecorder.onstart = function(e) {
					this.chunks = [];
					this.start = (new Date).getTime();
					document.getElementById('record-button').disabled = true;
					document.getElementById('stop-recording-button').disabled = false;
				};
				mediaRecorder.ondataavailable = function(e) {
					this.chunks.push(e.data);
					
					if((new Date).getTime()-this.start > 10000 && this.start != 0){
						console.log('stop');
						this.start = 0;
						mediaRecorder.stop();
					}

				};
				mediaRecorder.onstop = function(e) {
					var blob = new Blob(this.chunks, { 'type' : 'audio/ogg; codecs=opus' });
					roomService.sendAudio(blob);

					document.getElementById('record-button').disabled = false;
					document.getElementById('stop-recording-button').disabled = true;
				};

				mediaRecorder.start();
			});
	};

	$scope.stopRecording = function(){
		mediaRecorder.stop();
	}

	function subscribeEvents() {
		roomService.subscribeKeyPressed(digestDecorator(ovewriteOperations));
		roomService.subscribeErase(digestDecorator(addOperation));
		roomService.subscribeErase(digestDecorator(eraseOperation));
		roomService.subscribeClear(digestDecorator(clearOperations));
		roomService.subscribeSubmit(digestDecorator(submitOperations));
		roomService.subscribeAudio(digestDecorator(audioOperation));
	}

	function init() {
		var roomId = $routeParams.roomId;
		if(roomId) {

			roomService.joinRoom(roomId)
				.then(function() {
					subscribeEvents();
				});
		} else {
			roomService.createRoom()
				.then(function(roomId) {
					$location.update_path('/room/' + roomId);
					subscribeEvents();
				});
		}
	}

	init();
	new Clipboard('#navRoomButton');
});
