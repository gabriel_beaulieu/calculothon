angular.module("calculothon", ['ngRoute', 'ngLocationUpdate'])
.config(function ($routeProvider) {


	$routeProvider
		.when('/', {
			controller: 'homeController',
			templateUrl: '/app/template/home.template.htm'
		})
		.when('/room', {
			controller: 'roomController',
			templateUrl: '/app/template/room.template.htm'
		})
		.when('/room/:roomId', {
			controller: 'roomController',
			templateUrl: '/app/template/room.template.htm'
		})
		.otherwise({
			redirectTo: '/'
		});
});
