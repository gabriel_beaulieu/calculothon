angular.module("calculothon")
	.factory('roomService', function($http, $location, $q, socket) {

		function createRoom() {
			var deferred = $q.defer();
			$http.post('/room', {}).then(function(response) {
				socket.connect(response.data);
				deferred.resolve(response.data);
			}, function(response){
				deferred.reject(response.data);
				$location.path('/');
			});
			return deferred.promise;
		};

		function joinRoom(roomId) {
			var deferred = $q.defer();
			$http.put('/room/' + roomId, {}).then(function(response){
				socket.connect(response.data);
				deferred.resolve(response.data);
			}, function(response){
				deferred.reject(response.data);
				$location.path('/');
			});
			return deferred.promise;
		};

		function sendKeyPressed(key) {
			socket.emit('calculator.keypress', {key: key});
		};

		function sendErase() {
			socket.emit('calculator.erase');
		};

		function sendClear() {
			socket.emit('calculator.clear')
		};

		function sendSubmit() {
			socket.emit('calculator.submit');
		};

		function sendAudio(blob) {
			socket.emit('audio.send',blob);
		};

		function subscribeKeyPressed(callback) {
			socket.subscribe('calculator.keypress', callback);
		};

		function subscribeErase(callback) {
			socket.subscribe('calculator.erase', callback);
		};

		function subscribeClear(callback) {
			socket.subscribe('calculator.clear', callback);
		};

		function subscribeSubmit(callback) {
			socket.subscribe('calculator.submit', callback);
		};

		function subscribeAudio(callback){
			socket.subscribe('audio.send',callback);
		};

		return {
			joinRoom: joinRoom,
			createRoom: createRoom,
			sendKeyPressed: sendKeyPressed,
			sendErase: sendErase,
			sendClear: sendClear,
			sendSubmit: sendSubmit,
			sendAudio: sendAudio,
			subscribeKeyPressed: subscribeKeyPressed,
			subscribeErase: subscribeErase,
			subscribeClear: subscribeClear,
			subscribeSubmit: subscribeSubmit,
			subscribeAudio: subscribeAudio
		};
	});
