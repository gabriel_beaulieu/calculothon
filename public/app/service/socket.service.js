angular.module("calculothon")
	.factory('socket', function() {

		var socket;

		function connect(roomId) {
			socket = io.connect('https://localhost:4000', {secure: true, query: 'roomId=' + roomId});
		};

		function emit(event, message) {
			socket.emit(event, message);
		};

		function subscribe(event, callback) {
			socket.on(event, callback);
		};

		return {
			connect: connect,
			emit: emit,
			subscribe: subscribe
		};
	});
