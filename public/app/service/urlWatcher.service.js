angular.module('calculothon')
.run( function($rootScope, $location) {
   $rootScope.$watch(function() {
      return $location.path();
    },
    function (url){
      console.log(url)
      if(url == '/room' || url == '/') {
        $("#newRoomMenu").show();
        $("#shareRoomMenu").hide();
        $("#roomId").attr("value", "");
    	} else {
        $("#newRoomMenu").hide();
    		$("#shareRoomMenu").show();
    		$("#roomId").attr("value", "https://localhost:5000/#"+url);
    	}
    });
});
