const uuid = require('node-uuid');
const scheduler = require('node-schedule');

const modules = {
	redis: require('./module/redis.module'),
	express: require('./module/express.module'),
	websocket: require('./module/websocket.module')
}

process.env['CALCULOTHON_PROFILE'] = 'PRODUCTION';

modules.redis.init();
modules.express.init();
modules.websocket.init();

modules.redis.clear();

var roomToDelete = [];

scheduler.scheduleJob('*/30 * * * * *', function(){
	roomToDelete.forEach(function(roomId){
		modules.redis.deleteKey(roomId);
		modules.websocket.purgeSocket(roomId);
	});
	roomToDelete.length = 0;
});

function handleConnection(roomId){
	modules.redis.getValue(roomId)
		.then(function(room) {
			var roomProperties = room;
			if(roomProperties.users == 0 && roomToDelete.indexOf(roomId) != -1){
				roomToDelete.splice(roomToDelete.indexOf(roomId),1);
			}
			roomProperties.users++;
			return modules.redis.setValueProperty(roomId, roomProperties);
		}, function() {
			return false;
		});
}

function handleDisconnection(roomId){
	modules.redis.getValue(roomId)
		.then(function(room) {
			var roomProperties = room;
			roomProperties.users--;
			if(roomProperties.users <= 0){
				roomToDelete.push(roomId);
			}
			return modules.redis.setValueProperty(roomId, roomProperties);
		}, function() {
			return false;
		});
}

modules.websocket.handleConnection(handleConnection);
modules.websocket.handleDisconnection(handleDisconnection);

modules.express.registerPost('/room', function(req, res) {
	var roomId = uuid.v4();

	var roomProperties = {
		users: 0,
		timestamp: Math.floor(new Date() / 1000)
	}

	modules.redis.setValueProperty(roomId, roomProperties)
		.then(function() {
			res.status(201).send(roomId);
		}, function() {
			res.status(500).send("Could not create room session");
		});
});

modules.express.registerPut('/room/:roomId', function(req, res) {
	var roomId = req.params.roomId;

	if(!roomId.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/)) {
		res.status(500).send("Invalid roomId");
	}

	modules.redis.hasKey(roomId)
		.then(function() {
			return res.status(200).send(roomId);
		}, function() {
			return res.status(404).send("Room not found");
		});
});
