var sinon = require('sinon');
var assert = require('assert');

var express = require('express');
var https = require('https');

process.env['CALCULOTHON_PROFILE'] = 'TEST';

describe('express.module', function() {

	var expressModule;
	var mockedExpress;

	before(function() {

		var mockedConfigs = {
			express: {
				cert: 'lol'
			}
		};

		mockedExpress = {
			use: sinon.spy(),
			post: sinon.spy(),
			put: sinon.spy()
		};

		var configs = require('../conf/config');

		sinon.stub(configs, 'getConfigs', function() {return mockedConfigs});
		sinon.stub(https, 'createServer', function() {return {listen: sinon.stub()}});
		sinon.stub(require.cache[require.resolve('express')], 'exports', function() { return mockedExpress});

		expressModule = require('./express.module');
	});

	describe('#init()', function() {

		it('creates an express http server', function() {
			expressModule.init();
			sinon.assert.calledOnce(https.createServer);
		});
	});

	describe('#registerPost(route, callback)', function() {

		before(function() {
			expressModule.init();
		});

		it('registers a post route', function() {

			expressModule.registerPost('/route/du/demon', function() {return 'chose'});

			assert.equal('/route/du/demon', mockedExpress.post.getCall(0).args[0]);
			assert.equal('chose', mockedExpress.post.getCall(0).args[1]());
		});
	});

	describe('#registerPut(route, callback)', function() {

		before(function() {
			expressModule.init();
		});

		it('registers a post route', function() {

			expressModule.registerPut('/route/du/demon/2', function() {return 'chose2'});

			assert.equal('/route/du/demon/2', mockedExpress.put.getCall(0).args[0]);
			assert.equal('chose2', mockedExpress.put.getCall(0).args[1]());
		});
	});
});
