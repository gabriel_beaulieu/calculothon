const express = require('express');
const https = require('https');

const logger = require('./logger.module');

const config = require('../conf/config').getConfigs().express;

var app;

function init() {
	app = express();
	app.use(express.static('public'));
		https.createServer(config.cert, app).listen(config.port, function(){
		logger('🌍', `Express listening on port ${config.port}`);
	});
}

function registerPost(route, callback) {
	logger('🌍', `Registered route POST ${route}`);
	app.post(route, callback);
}

function registerPut(route, callback) {
	logger('🌍', `Registered route PUT ${route}`);
	app.put(route, callback);
}

module.exports = {
	init: init,
	registerPost: registerPost,
	registerPut: registerPut
}
