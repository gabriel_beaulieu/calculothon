const redis = require('redis');
const Q = require('Q');

const logger = require('./logger.module');

var redisClient = redis.createClient();

function init() {
	redisClient.on('connect', function() {
		logger('🍎', `Redis connected`);
	});

	redisClient.on('ready', function() {
		logger('🍎', `Redis ready`);
	});

	redisClient.on('error', function(err) {
		logger('🍎', `Redis error`, err);
	});

	redisClient.on('warning', function(warn) {
		logger('🍎', `Redis warn`, warn);
	});
}

function clear(key) {
	redisClient.keys("*", function (err, keys) {
		redisClient.del(keys, function(err, o) {
		});
	});
}

function deleteKey(key) {
	var deferred = Q.defer();
	redisClient.del(key, function(err, o) {
		if(err) {
			return deferred.reject(err);
		}
		deferred.resolve(o);
	});
	return deferred.promise;
}

function getValue(key) {
	var deferred = Q.defer();
	hasKey(key).then(function(data){
		redisClient.hgetall(key, function(err, list) {
			if(err) {
				return deferred.reject(err);
			}
			deferred.resolve(list);
		});
	}, function(data){
		return deferred.reject();
	});
	return deferred.promise;
}

function getValueProperty(key, value) {
	var deferred = Q.defer();
	redisClient.hmget(key, value, function(err, list) {
		if(err) {
			return deferred.reject(err);
		}
		deferred.resolve(list);
	});
	return deferred.promise;
}

function hasKey(key) {
	var deferred = Q.defer();
	redisClient.exists(key, function(err, list) {
		if(err || !list) {
			return deferred.reject(false);
		}
		deferred.resolve(true);
	});
	return deferred.promise;
}

function setValueProperty(key, value) {
	var deferred = Q.defer();
	redisClient.hmset(key, value, function(err){
		if(err) {
			return deferred.reject(err);
		}
		deferred.resolve();
	});
	return deferred.promise;
}

module.exports = {
	init: init,
	clear: clear,
	deleteKey: deleteKey,
	getValue: getValue,
	getValueProperty: getValueProperty,
	hasKey: hasKey,
	setValueProperty: setValueProperty
}
