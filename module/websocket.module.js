const socketio = require('socket.io');
const mathjs = require('mathjs');
const https = require('https');

const config = require('../conf/config').getConfigs().websocket;

var connectionCallback;
var disconnectionCallback;
var calculator = {};

var init = function() {
	var server = https.createServer(config.cert);
	var io = socketio(server);

	io.on('connection', function(socket) {

		var roomId = socket.handshake.query.roomId;
		socket.join(roomId);
		connectionCallback(roomId);

		console.log(`📡  ====> Client connected to room ${roomId}`);

		if(!calculator[roomId]){
			calculator[roomId] = '';
		}
		else {
			console.log(`📡  ====> New client from room ${roomId} received '${calculator[roomId]}'`);
			socket.emit('calculator.keypress', {action: 'keypress', operations: calculator[roomId]});
		}

		socket.on('audio.send', function(data){
			console.log(`🎙  ====> Audio recording sent to users.`);
			io.to(roomId).emit('audio.send', data);
		});

		socket.on('calculator.keypress', function(data) {
			calculator[roomId] += data.key;
			console.log(`📡  ====> Room ${roomId} received operation '${data.key}' (${calculator[roomId]})`);
			io.to(roomId).emit('calculator.keypress', {action: 'keypress', operations: calculator[roomId]});
		});

		socket.on('calculator.submit', function(data) {
			var calculation = calculator[roomId];
			var result;
			
			try{
				result = mathjs.eval(calculation)
			} catch(e) { 
				result = "ERR";
			}

			calculator[roomId] = '';
			console.log(`📡  ====> Room ${roomId} submitted operations ${calculation} = ${result}`);
			io.to(roomId).emit('calculator.submit', {operations: calculation, result: result});
		});

		socket.on('calculator.erase', function(data) {
			calculator[roomId] = calculator[roomId].substring(0, calculator[roomId].length - 1);
			console.log(`📡  ====> Room ${roomId} erased last operation (${calculator[roomId]})`);
			io.to(roomId).emit('calculator.erase');
		});

		socket.on('calculator.clear', function(data) {
			calculator[roomId] = '';
			console.log(`📡  ====> Room ${roomId} cleared the operations (${calculator[roomId]})`);
			io.to(roomId).emit('calculator.clear');
		});

		socket.on('disconnect', function(data){
			disconnectionCallback(roomId);
		});
	});

	server.listen(config.port, function(){
		console.log(`📡  ====> Socket listening on port ${config.port}`);
	});
}

function handleConnection(callback){
	connectionCallback = callback;
}

function handleDisconnection(callback){
	disconnectionCallback = callback;
}

function purgeSocket(roomId){
	delete calculator[roomId];
}

module.exports = {
	init: init,
	handleConnection: handleConnection,
	handleDisconnection: handleDisconnection,
	purgeSocket: purgeSocket
}
