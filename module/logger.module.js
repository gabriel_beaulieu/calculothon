module.exports = function(emoji, message, extra) {

	if(process.env.CALCULOTHON_PROFILE !== 'TEST') {
		if(extra) {
			console.log(`${emoji}  ====> ${message}`, extra);
		} else {
			console.log(`${emoji}  ====> ${message}`);
		}
	}
};
