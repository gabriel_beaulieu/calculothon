var sinon = require('sinon');
var assert = require('assert');

describe('logger.module()', function() {

	var logger;

	beforeEach(function() {

		sinon.spy(console, 'log');
		logger = require('./logger.module');
	});

	afterEach(function() {
		console.log.restore();
	});

	it('displays the log when not in TEST profile', function() {
		process.env['CALCULOTHON_PROFILE'] = 'PRODUCTION';
		logger('🍆', 'Salut');
		sinon.assert.calledWith(console.log, '🍆  ====> Salut');
	});

	it('doesn\'t displays the log when in TEST profile', function() {
		process.env['CALCULOTHON_PROFILE'] = 'TEST';
		logger('🍆', 'Salut');
		sinon.assert.notCalled(console.log);
	});

	it('displays the extra when not in TEST profile', function() {
		process.env['CALCULOTHON_PROFILE'] = 'PRODUCTION';
		logger('🍆', 'Salut', 'lol');
		sinon.assert.calledWith(console.log, '🍆  ====> Salut', 'lol');
	});
});
