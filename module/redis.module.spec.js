var sinon = require('sinon');
var assert = require('assert');

describe('redis.module', function() {

	var redisModule;
	var mockedRedisClient;

	before(function() {

		var redis = require('redis');
		mockedRedisClient = {
			on: sinon.spy(),
			hmget: sinon.spy(),
			hmset: sinon.spy()
		};
		sinon.stub(redis, 'createClient', function() {return mockedRedisClient});

		redisModule = require('./redis.module');
	});

	describe('#init()', function() {

		it('initiates all redis events', function() {

			redisModule.init();
			sinon.assert.calledWith(mockedRedisClient.on, 'connect');
			sinon.assert.calledWith(mockedRedisClient.on, 'ready');
			sinon.assert.calledWith(mockedRedisClient.on, 'error');
			sinon.assert.calledWith(mockedRedisClient.on, 'warning');
		});
	});

	describe('#getValueProperty(key, value)', function() {

		it('calls hmget with `allo` and `bollo`', function() {

			redisModule.getValueProperty('allo', 'bollo');
			sinon.assert.calledWith(mockedRedisClient.hmget, 'allo', 'bollo');
		});
	});

	describe('#setValueProperty(key, value)', function() {

		it('calls hmset with `salut` and `lulu`', function() {

			redisModule.setValueProperty('salut', 'lulu');
			sinon.assert.calledWith(mockedRedisClient.hmset, 'salut', 'lulu');
		});
	});
});
