const fs = require('fs');

module.exports.getConfigs = function() {
	return {
		profile: process.env.CALCULOTHON_PROFILE,
		websocket: {
			port: process.env.CALCULOTHON_WEBSOCKET_PORT || 4000,
			cert: {
				key: fs.readFileSync('server.key'),
				cert: fs.readFileSync('server.crt'),
				requestCert: false,
				rejectUnauthorized: false
			}
		},
		express: {
			port: process.env.CALCULOTHON_EXPRESS_PORT || 5000,
			cert: {
				key: fs.readFileSync('server.key'),
				cert: fs.readFileSync('server.crt'),
				requestCert: false,
				rejectUnauthorized: false
			}
		}
	}
};
